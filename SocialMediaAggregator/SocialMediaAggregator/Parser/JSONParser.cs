﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.Json;

namespace SocialMediaAggregator.Parser
{
    public class JSONParser<T>
    {
        public T parseJSON(string json)
        {
            return JsonSerializer.Deserialize<T>(json);
        }
    }
}
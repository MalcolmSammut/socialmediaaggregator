﻿using Antlr.Runtime;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using SocialMediaAggregator.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SocialMediaAggregator.Controllers
{
    [Authorize]

    public class FacebookController : Controller
    {
        public static string email
        {
            get; set;
        }
        private ApplicationDbContext db = new ApplicationDbContext();

        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult preferance()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult preferance(bool status)
        {
            try
            {
                Email f = db.email.Where(x => x.user == email).FirstOrDefault();
                f.status = status;
                db.SaveChanges();
                return RedirectToAction("Posts");
            }
            catch
            {
                return View();
            }
        }

        public async Task<ActionResult> Posts()
        {
            var id = User.Identity.GetUserId();

            var currentClaims = await UserManager.GetClaimsAsync(HttpContext.User.Identity.GetUserId());
            var accesstoken = currentClaims.FirstOrDefault(x => x.Type == "urn:tokens:facebook");
            if (accesstoken == null)
            {
                return (new HttpStatusCodeResult(HttpStatusCode.NotFound, "Token not found"));
            }
            string url = String.Format("https://graph.facebook.com/me?fields=id,picture.type(large),name,email,posts.fields(picture,message),likes,albums.fields(photos.fields(source)),feed.fields(picture,message)&redirect=false&redirect=false&access_token={0}", accesstoken.Value);
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "GET";
            using (HttpWebResponse response = await request.GetResponseAsync() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string result = await reader.ReadToEndAsync();
                FacebookModel facebook = new JavaScriptSerializer().Deserialize<FacebookModel>(result);
                ViewBag.id = facebook.id;
                ViewBag.name = facebook.name;
                ViewBag.email = facebook.email;
                ViewBag.feed = facebook.feed.data;
                ViewBag.likes = facebook.likes.data;
                ViewBag.posts = facebook.posts.data;
                ViewBag.albums = facebook.albums.data;
                email = facebook.email;
                if (db.email.Where(x => x.user == facebook.email).Count() == 0)
                {
                    Email e = new Email();
                    e.user = facebook.email;
                    e.status = true;
                    db.email.Add(e);
                    db.SaveChanges();
                }
                Email f = db.email.Where(x => x.user == email).FirstOrDefault();
                ViewBag.status = f.status;

            }

            return View();
        }
    }
}
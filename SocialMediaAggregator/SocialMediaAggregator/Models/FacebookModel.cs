﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace SocialMediaAggregator.Models
{
    public class FacebookModel
    {
        public string id { get; set; }
        public Data3 picture { get; set; }
        public string name { get; set; }
        public Posts posts { get; set; }

        public Likes likes { get; set; }
        public Albums albums { get; set; }
        public Feed feed { get; set; }

        public Hometown hometown { get; set; }
        public string email { get; set; }
    }

    public class Hometown
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Data3
    {
        public int height { get; set; }
        public string is_silhouette { get; set; }
        public string url { get; set; }
        public int width { get; set; }
    }

    public class Posts
    {
        public List<Data> data { get; set; }
    }

    public class Data
    {
        public string picture { get; set; }
        public string message { get; set; }
        public string id { get; set; }
    }

    public class Likes
    {
        public List<Data2> data { get; set; }
    }

    public class Data2
    {
        public string name { get; set; }
        public string id { get; set; }
        public string created_time { get; set; }
    }
    public class Albums
    {
        public List<Data6> data { get; set; }
    }

    public class Data6
    {
        public List<Photos> photos { get; set; }
    }

    public class Photos
    {
        public List<Data4> data { get; set; }
    }

    public class Data4
    {
        public string source { get; set; }
        public string id { get; set; }
    }

    public class Feed
    {
        public List<Data> data { get; set; }

    }
}
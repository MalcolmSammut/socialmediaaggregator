﻿using Antlr.Runtime.Tree;
using Evernote.EDAM.Type;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SocialMediaAggregator.Models
{

    [Table("Email", Schema = "dbo")]
    public class Email
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public string user { get; set; }

        public bool status { get; set; }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SocialMediaAggregator.Startup))]
namespace SocialMediaAggregator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
